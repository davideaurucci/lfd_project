#!/usr/bin/env python

import os
import sys
import time
import config
import features
import itertools
import read_data
import evaluation
import prepare_corpus
import preprocess_data
import pos_tagging
import train_model
import preparetestdata
import test_model
import train_and_predict
import output

from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion

def identity(x):
    return list(itertools.chain.from_iterable(x))

def createList(X, y, pos):  
    '''Helper function for creatling lists instead of dictionaries'''
    Xlist, ylist, user_ids, poslist = [],[],[],[]
    for user_id, tweets in X.items():
        ylist.append(y[user_id])
        Xlist.append(tweets)
        poslist.append(pos[user_id])
        user_ids.append(user_id)
    return Xlist, ylist, user_ids, poslist

def main(language):
    TESTMODE = False  # Set to true to test on another dataset    
    TOKENIZE = False  # Set to true to tokenize the dataset
    
    # 1) Prepare the corpus
    prepare_corpus.prepare()

    # 2, 3) Read & preprocess data
    Xtrain, ytrain = read_data.read_train_data(language)
    Xtrain = preprocess_data.preprocess(Xtrain, language, TOKENIZE, False)
    pos_train, pos_test = read_data.read_pos(language), []
    Xtrain, ytrain, user_id, pos_train = createList(Xtrain, ytrain, pos_train)
    
    if TESTMODE:
        Xtest, ytest = preparetestdata.read_data(language)
        Xtest = preparetestdata.preprocess(Xtest, language, TOKENIZE, True)
        pos_test = read_data.get_pos(language, Xtest)
        Xtest, ytest, user_id, pos_test = createList(Xtest, ytest, pos_test)

    # 4) Extract features
    pipeline = Pipeline([
    ('features', FeatureUnion([
		('EmoticonNoses', features.EmoticonNoses()),
		('EmoticonReverse', features.EmoticonReverse()),
		('EmoticonCount', features.EmoticonCount()),
		('EmoticonEmotion', features.EmoticonEmotion()),
        ('StartsWithCapital', features.StartsWithCapital()),
        ('EndsWithPunctuation', features.EndsWithPunctuation()),
        ('AverageWordLength', features.AverageWordLength()),
        ('AverageSentenceLength', features.AverageSentenceLength()),
        #('Misspell', features.Misspell(language)),
        ('PunctuationByTweet', features.PunctuationByTweet()),
        ('CapitalizedTokens', features.CapitalizedTokens()),
        ('CapitalLetters', features.CapitalLetters()),
        ('VocabularyRichness', features.VocabularyRichness()),
        ('wordvec', TfidfVectorizer(ngram_range = (1,3), preprocessor = lambda x: x, tokenizer = identity)),
        ('charvec', TfidfVectorizer(analyzer = 'char', ngram_range = (2,5), preprocessor = lambda x: ' '.join(identity(x)))),
        ('posvecpipeline', Pipeline([('posvec', features.PosVec(Xtrain, pos_train, pos_test)), ('tfidf', TfidfVectorizer(ngram_range = (2,4)))])),
        ('FunctionWords', features.FunctionWords(2500)),
    ])),
    ('classifier', LinearSVC())
    ])

    # 5) Evaluate
#   for label in config.LABELS:
#    if TESTMODE:
#        y, prediction = test_model.test_model(pipeline,Xtrain,ytrain,Xtest,ytest,language,label)
#        evaluation.evaluate(y,prediction, language, label)
#    else:
#          y, prediction = train_model.train_model(pipeline,Xtrain,ytrain,language,label)
#          evaluation.evaluate(y,prediction, language, label)

# the code commented above was changed to the code below, but it does exactly the same thing
# I just had to change it so we have the predictions for gender and for age groups in a variable, so I can output the files

    if TESTMODE:
        yGender, predictionGender = test_model.test_model(pipeline,Xtrain,ytrain,Xtest,ytest,language,'gender')
        evaluation.evaluate(yGender,predictionGender, language, 'gender')
        yAgeGroup, predictionAgeGroup = test_model.test_model(pipeline,Xtrain,ytrain,Xtest,ytest,language,'ageGroup')
        evaluation.evaluate(yAgeGroup,predictionAgeGroup, language, 'ageGroup')
    else:
        yGender, predictionGender = train_model.train_model(pipeline,Xtrain,ytrain,language,'gender')
        evaluation.evaluate(yGender,predictionGender, language, 'gender')
        yAgeGroup, predictionAgeGroup = train_model.train_model(pipeline,Xtrain,ytrain,language,'age_group')
        evaluation.evaluate(yAgeGroup,predictionAgeGroup, language, 'age_group')

#        output.output_prediction_files(user_id, language, predictionAgeGroup, predictionGender, '/home/nissim16/pan16-author-profiling/temp/'+language) 
        

    
