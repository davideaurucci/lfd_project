#!/usr/bin/python

import sys, codecs, os, glob, itertools
import xml.etree.ElementTree as ET

#sys.stdout = codecs.getwriter('utf8')(sys.stdout)
#sys.stderr = codecs.getwriter('utf8')(sys.stderr)

def check_duplicates():
    trainpath = 'dutch'
    set_truth_additional = set()
    set_truth = set()
    duplicates = []

    #read correct labels from truth file and append them to labels list
    with open(trainpath + '/truth_additional.txt', encoding='utf-8') as truth:
        for line in truth:
            items = line.split(":::")
            set_truth_additional.add(items[0])

    with open(trainpath + '/truth.txt', encoding='utf-8') as truth:
        for line in truth:
            items = line.split(":::")
            set_truth.add(items[0].replace("-",""))

    for doc in set_truth:
        if doc in set_truth_additional:
            duplicates.append(doc)

    return duplicates


def main():
    duplicates = check_duplicates()
    print(duplicates)
main()
