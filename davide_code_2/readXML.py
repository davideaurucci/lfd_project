#!/usr/bin/python
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, f1_score, classification_report, confusion_matrix, precision_score
from sklearn import svm
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.naive_bayes import MultinomialNB

from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
import sys, codecs, os, glob, itertools, time
import xml.etree.ElementTree as ET
import nltk
import re
import operator
from collections import Counter,defaultdict


#READ THE XML FILES FROM THE TRAINING DIRECTORY
def read_xml(use_gender):
    #To do: tokenize
    trainpath = 'training/english'
    documents = []
    labels = []
    documents_id = {}
    #FIRST ITERATION ON ALL XML FILES --> Every file is a different author
    for file in sorted(glob.glob(os.path.join(trainpath, '*.xml'))):
        
        #LIST WHERE WE'LL HAVE ALL THE TOKENS OF AN XML FILE
        authorlist = []
        
        filename = file[17:-4]
        
        #CLEANING OF XML FILE --> REMOVAL OF ALL TAGS --> WE'LL HAVE ONLY TEXT
        tree = ET.parse(file)
        root = tree.getroot()
        #[item for sublist in l for item in sublist]
        
        #SECOND ITERATION ON ALL THE TWEETS OF A XML FILE --> TOKENIZATION
        for tweet in root.iter('document'):
            tokens = tweet.text.strip().split()
            authorlist.append(tokens)
        documents_id[filename] =  authorlist

        #flatten list and add to list documents
        documents.append(list(itertools.chain(*authorlist)))

    #read correct labels from truth file and append them to labels list
    with open(trainpath + '/truth.txt', encoding='utf-8') as truth:
        for line in truth:
            items = line.split(":::")
            #print(items[1])
            if use_gender: labels.append(items[1])
            else: labels.append(items[2])

    return documents, labels, documents_id



#TERM FREQUENCY IN A LIST
def count_words(documents):
    counts = Counter(documents)
    print(counts)


#REPLACE WORDS:
def replace_word(documents):
    authors_final =[]
    document_final = []
    for authors in documents:
        for words in authors:
            if ('http' in words):
                x = words.replace(words, 'LINK')
            elif ('????' in words):
                x = words.replace(words, 'QUESTION')
            elif ('username' in words):
                x =words.replace(words, 'USERNAME')
            else:
                x = words
            authors_final.append(x)
        document_final.append(authors_final)
        authors_final = []
    return document_final



#Modify the dictionary documents_id
def clean_dictionary(documents_id):
    aux1 = []
    aux2 = []
    for key in documents_id:
        for words in documents_id[key]:
            for w in words:
                aux1.append(w.lower())
        documents_id[key] = aux1
        aux1 = []

    for k in documents_id:
        for w in documents_id[k]:
            if ('http' in w):
                x = w.replace(w, 'LINK')
            elif ('????' in w):
                x = w.replace(w ,'QUESTION')
            elif ('username' in w):
                x =w.replace(w, 'USERNAME')
            else:
                x = w
            aux2.append(x)
        documents_id[k] = aux2
        aux2 =[]
    return documents_id



#Get most X frequent words for the whole set of tweets
def get_words_feature(authors_all_together):
    all_words = Counter(authors_all_together)
    sorted_all_words = sorted(all_words.items(), key=operator.itemgetter(1), reverse=True)
    word_features = set()
    for key in sorted_all_words[:1500]:
        word_features.add(key[0])
    return word_features




#FEATURE EXTRACTOR FUNCTION --> I consider if the tweets of an author contain all the most 1000 frequent words
'''
def feature_extractor(documents_id, word_features, user_id, gender):
    final_result = []
    authors = get_tweets_from_user(user_id,documents_id)
    for words in authors:
        #final_result.append((words,(words in word_features)))
        if words in word_features:
            final_result.append(words)
    #print(final_result)
    return final_result
'''
def feature_extractor(word_features,documents_free):
    final_result = []
    authors = []
    for author in documents_free:
        for word in author:
            if word in word_features:
                authors.append(word)
        final_result.append(authors)
        authors = []
    print(final_result)
    return final_result



#Get all the user id
def get_all_user_id():
    trainpath = 'training/english'
    all_user_id = []
    with open(trainpath + '/truth.txt', encoding='utf-8') as truth:
        for line in truth:
            items = line.split(":::")
            all_user_id.append(items[0])
        return all_user_id



#Get the list of tweets of a specific user
def get_tweets_from_user(id, documents_id):
    return (documents_id[id])


def identity(x):
    return x


#MAIN
def main():
    #True: Gender prediction / False: Age Prediction
    use_gender = False
    documents, labels, documents_id = read_xml(use_gender)
    authors_lower = []
    documents_lower = []
    #CLEAN THE DOCUMENTS --> lower case and replace of link, question and username
    for authors in documents:
        for words in authors:
            authors_lower.append(words.lower())
        documents_lower.append(authors_lower)
        authors_lower = []
    documents_free = replace_word(documents_lower)
    cleaned_documents_id = clean_dictionary(documents_id)
    #We need a new list of all the tweets of all the authors together in a single list
    authors_all_together =[]
    for authors in documents_free:
        for word in authors:
            authors_all_together.append(word)
    #print(cleaned_documents_id)


    #FEATURE EXTRACTION
    #First we need the 1000 most frequent words used in the tweets (ENGLISH/SPANISH/DUTCH/ITALIAN)
    word_features = get_words_feature(authors_all_together)
    #feature_set = [feature_extractor(documents_id, word_features, get_all_user_id()[i],labels[i]) for i in range(0,len(labels))]
    feature_set = feature_extractor(word_features,documents_free)
    #feature_set = documents_free
    split_point = int(0.75*len(feature_set))
    Xtrain = feature_set[:split_point]
    print(Xtrain)
    Ytrain = labels[:split_point]
    Xtest= feature_set[split_point:]
    Ytest= labels[split_point:]

    # let's use the TF-IDF vectorizer
    tfidf = True
    if tfidf:
        vec = TfidfVectorizer( preprocessor = identity, tokenizer = identity)
                              #,ngram_range=(1, 5), analyzer = 'word')
                           #stop_words = {'english'})
    else:
        vec = CountVectorizer(preprocessor = identity, tokenizer = identity)
    # HashingVectorizer
        hv = HashingVectorizer(preprocessor = identity, tokenizer = identity)




    # combine the vectorizer with a SVM Classifier
    classifier = Pipeline( [ #('features', FeatureUnion([
                        ('vec', vec),
                        #('hv', hv),
                        # NAIVE-BAYES
                            #('cls', MultinomialNB())] )
                        # SVM Classifier
                        ('cls', svm.SVC(kernel='linear', C=2.5))]) #SVM linear
                        #('cls', svm.SVC(kernel='rbf', gamma=0.1, C=10))]) #SVM non-linear
                        #DECISION TREES
                        #('cls', DecisionTreeClassifier(min_samples_leaf= 2))])
                        #('cls', DecisionTreeClassifier(max_depth= X))])
                        #('cls', DecisionTreeClassifier(min_samples_leaf= X))])
                        #('cls', KNeighborsClassifier(n_neighbors=20))])


    # Traindata and labels are put in the classifier to create a predictive model.
    # Predicted labels for testdata are then assigned to Yguess. Time for training and testing is calculated.






    #COMPUTATION OF TRAINING TIME
    t0 = time.time()
    classifier.fit(Xtrain, Ytrain)
    train_time = time.time() - t0
    print("\n")
    print("TRAINING time: ", train_time)

    #COMPUTATION OF TESTING TIME
    t1 = time.time()
    Yguess = classifier.predict(Xtest)
    test_time = time.time()-t1
    print("TESTING time: ", test_time)


    print("\n")
    print("ACCURACY SCORE\n", accuracy_score(Ytest, Yguess))
    print("\n")
    print("PRECISION SCORE\n", precision_score(Ytest, Yguess,average='weighted'))
    print("\n")
    # Print classification report with p, r, f1 per class
    print("Classification Report:\n", classification_report(Ytest, Yguess))
    print("\n")
    print("CONFUSION MATRIX\n", confusion_matrix(Ytest, Yguess))

    #FUNCTION TO COMPUTE THE CROSS VALIDATION
    scores = cross_val_score(classifier, Xtest, Ytest, cv=5)

    print("CROSS VALIDATION: Mean Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    print("\n")



main()
