#!usr/bin/python3

from sklearn.base import BaseEstimator, TransformerMixin


class FirstOrder(TransformerMixin):
    '''Parent class for second order real valued features'''
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        newX = [[self.calculate(x)] for x in X]
        #print("OldX:\n", X)
        #print("NewX:\n", newX)
        return newX



        
class CapitalLetters(FirstOrder):
    '''Average of percentage of capitalized letters per tweet'''
    def calculate(self, user_tweets):
        average_count = 0
        
        for user_tweet in user_tweets:
            if len(user_tweet) > 0:
                capital_letter_count = 0
                number_of_characters = 0
                for tok in user_tweet:
                    number_of_characters += len(tok)
                    for character in tok:
                        if character.isupper():
                            capital_letter_count += 1
                average_count += capital_letter_count / number_of_characters
                
        result = average_count/len(user_tweets)
        print("Result Capital:", result)
        return result


class TweetURL(FirstOrder):
    '''Average of percentage of capitalized letters per tweet'''
    def calculate(self, author_tweets):
        average_count = 0
        url_count = 0

        for token in author_tweets:
            if len(token) > 0:
                number_of_characters = 0
                #print(token)
                if token[:7] == 'http://':
                    url_count += 1
        

                
        #result = average_count/len(author_tweets)
        print("# of URLs:", url_count)
        return url_count
        
        
        
class MaleWords(FirstOrder):
    def calculate(self, author_tweets):
        word_count = 0
        male_words = ['wrestling', 'director', 'ran', 'producer', 'dad', 'niggas', 'burn', 'father', 'actor', 'allah']
        
        for token in author_tweets:
            if len(token) > 0:
                if token in male_words:
                    word_count += 1
        
        #print("Wordcount male:", word_count)
        return word_count


class FemaleWords(FirstOrder):
    def calculate(self, author_tweets):
        word_count = 0
        male_words = ['camgirl', 'makeup', 'hurry', 'actress', 'communication', 'I', 'mommy', 'yours', 'dress', 'female', 'feminist']
        
        for token in author_tweets:
            if len(token) > 0:
                if token in male_words:
                    word_count -= 1
        
        #print("Wordcount female:", word_count)
        return word_count
        
        
        
