#!/usr/bin/python

import sys, codecs, os, glob, itertools
import xml.etree.ElementTree as ET

#sys.stdout = codecs.getwriter('utf8')(sys.stdout)
#sys.stderr = codecs.getwriter('utf8')(sys.stderr)

def read_xml():
    #To do: tokenize
    trainpath = 'training/english'
    documents = []
    labels = []
    
    #read all xml files from folder trainpath
    for file in sorted(glob.glob(os.path.join(trainpath, '*.xml'))):
        authorlist = []
        filename = file[17:-4]
        print(filename)
    
        tree = ET.parse(file)
        root = tree.getroot()
        #print(root.tag)
        #[item for sublist in l for item in sublist]

        for tweet in root.iter('document'):
            tokens = tweet.text.strip().split()
            authorlist.append(tokens)
            #print(tweet.text)
        
        #flatten list and add to list documents
        documents.append(list(itertools.chain(*authorlist)))

    #read correct labels from truth file and append them to labels list
    with open(trainpath + '/truth.txt', encoding='utf-8') as truth:
        for line in truth:
            items = line.split(":::")
            #print(items[1])
            labels.append(items[1])
    
    return documents, labels
    


# def read_corpus(corpus_file, use_sentiment):
    # documents = []
    # labels = []
    # with open(corpus_file, encoding='utf-8') as f:
        # for line in f:
            # tokens = line.strip().split()

            # documents.append(tokens[3:])

            # if use_sentiment:
                # # 2-class problem: positive vs negative
                # labels.append( tokens[1] )
            # else:
                # # 6-class problem: books, camera, dvd, health, music, software
                # labels.append( tokens[0] )

    # return documents, labels


def main():
    documents, labels = read_xml()
    print(len(documents))
    print(documents[0], file=sys.stdout)
    print(labels)
main()
